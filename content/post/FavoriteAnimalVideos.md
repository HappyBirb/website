---
title: Favorite animal videos
date: 2020-01-01
hero: "/images/Elephant.jpg"
excerpt: an accumulative collection of my favorite animal videos
authors:
  -  Shu

---

#

#### searching for and collecting the best animal videos.
&nbsp;
#
&nbsp;
#
{{< youtube qPDZZj6By3Y >}}
#
{{< youtube eIt0ub7PhWY >}}
#
{{< youtube VMSTzfF3udY>}}
#
{{< youtube gGludGaPKag >}}
#
{{< youtube RuLUdd9QyOM >}}
#
{{< youtube 8wl8ZxAaB2E >}}
#
{{< youtube 0mgnf6t9VEc >}}
#
{{< youtube IZTSd-CMEno >}}


<!-- 
Image:
![GitHub Logo](/images/logo.png)
Format: ![Alt Text](url)

If your video link is:
https://www.youtube.com/watch?v=w7Ft2ymGmfc
{{< youtube w7Ft2ymGmfc >}}  -->