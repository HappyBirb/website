---
title: Biomechanics of human pregnancy
date: 2021-02-21
hero: "/images/MechHumanUT.png"
excerpt: mechanical properties of the human uterus & finite element
authors:
  -  Shu

---

*joint work with [James McLean](https://structurefunctionlab.ee.columbia.edu/people/james-patrick-mclean), [Lei Shi](https://kristinmyerscolumbia.com/people/lei-shi/), [Joy Vink](https://www.columbiaobgyn.org/profile/joy-sarah-y-vink-md), [Christine Hendon](https://www.engineering.columbia.edu/faculty/christine-hendon) and [Kristin Myers](https://www.me.columbia.edu/faculty/kristin-myers)*
#
The human uterus grows and stretches to many times its size during gestation, and its mechanical function is critical for a successful pregnancy. To establish a rigorous testing protocol for the human uterus in hopes of predicting tissue stretch during pregnancy, we recruited human subjects and measured the uterine anisotropic mechanical properties. We first use optical coherence tomography to investigate the uterine fiber structure; we then perform spherical indentation, uniaxial tension, and digital image correlation to obtain the tissue's force and deformation responses to various loading regimes; finally we characterize the material properties using a constitutive model and inverse finite element analysis. We have found, based on our indentation tests, compared to NP uterine tissue, PG tissue has a more dispersed fiber distribution and equivalent stiffness material parameters. In both PG and NP uterine tissue, the mechanical properties differ significantly between anatomical locations. 

#### publications
 [Anisotropic material properties of the human uterus measured by spherical indentation](https://link.springer.com/epdf/10.1007/s10439-021-02769-0?sharing_token=EkfJSxzins_E1XmQRVKJUPe4RwlQNchNByi7wbcMAY5-81g5RaLPPxDMfmYc5BcDWVof3NH6skDxlVvR9FH8XgqPsWSmxfr5UNSK1PNfaIlNFtGJ_Zr2IcxprwXx5lUTVRpWnJiVF1cUULuDMcfBylMgWZnxqDb00-sAlgDD10s%3D)  
 `<working paper>` &nbsp; Mechanical properties of the human uterus measured by uniaxial tension