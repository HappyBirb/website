---
title: Mechanics of engineered human skin
date: 2021-04-01
hero: "/images/EngineeredSkin.jpg"
excerpt: tissue engineering & biomechanics
authors:
  -  Shu

---
*joint work with [Abby Herschman](https://www.linkedin.com/in/abby-herschman-53b906121), [Alberto Pappalardo](https://www.linkedin.com/in/alberto-pappalardo-75828212b), and [Hasan Erbil Abaci](https://www.dermatology.columbia.edu/profile/hasan-e-abaci-phd)*
#
As the largest organ of our human body, skin provides us with strong protection, critical regulation, and phenomenal sensation. Tissue engineering of human skin, aiming at reconstructing the structural and functional components, improves the quality of wound healing and scar formation. To evaluate the mechanical performance of the conventional engineered skin and wearable engineered skin developed by *Alberto* and *Erbil*, we perform a series of uniaxial tension tests on these two skin grafts from different tissue-culturing timepoints; we combine the mechanical tests with video extensometry to eliminate the geometric effect and to capture the material's stress response; we then use second-harmonic generation microscopy to investigate the tissue's fiber structure and analyze its mechanical performance physiologically. With this study, we hope to shorten, if not to bridge, the gap of the mechanical properties between the engineered and real skin.